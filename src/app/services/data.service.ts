import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { List } from './../interfaces/list.interface'
import { Card } from './../interfaces/Card.interface'

@Injectable()
export class DataService {
  private _lists = new BehaviorSubject<List[]>([]);

  constructor() {  
    
    this.lists = [
      {
        _id: 'rand1',
        title: 'todo',
        cards: [{
          _id: 'randID2',
          title: 'do something',
          content: 'bla bla'
        }, {
          _id: 'randID3',
          title: 'do something',
          content: 'bla bla'
        }]
      }
    ]
  }
  set lists(value) {
    this._lists.next(value);
  };

  get lists() {
    return this._lists.getValue();
  }

  addCard(list: List, card:Card) {
    let _listIdx = this.lists.findIndex((lst) => lst._id === list._id)
    this.lists[_listIdx].cards.push(card);
  }

  removeCard(list: List, card: Card) {
    let _listIdx = this.lists.findIndex((lst) => lst._id === list._id)
    let cardIdx = this.lists[_listIdx].cards.findIndex((crd)=> crd._id === card._id)
    this.lists[_listIdx].cards.splice(cardIdx, 1);
  }

  addList(list:List) {
    this.lists.push(list);
  }

  onListChanges = this._lists;

  uuidv4():string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
