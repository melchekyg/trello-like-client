import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCard.DialogComponent } from './add-card.dialog.component';

describe('AddCard.DialogComponent', () => {
  let component: AddCard.DialogComponent;
  let fixture: ComponentFixture<AddCard.DialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCard.DialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCard.DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
