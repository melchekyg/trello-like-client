import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';


import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { BoardComponent } from './components/board/board.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { DataService } from './services/data.service';
import { AddCardDialogComponent } from './dialogs/add-card-dialog/add-card-dialog.component';
import { AddListDialogComponent } from './dialogs/add-list-dialog/add-list-dialog.component';

import { DragDropDirectiveModule} from 'angular4-drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    BoardComponent,
    NavbarComponent,
    AddCardDialogComponent,
    AddListDialogComponent
  ],
  entryComponents: [
    AddCardDialogComponent,
    AddListDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    DragDropDirectiveModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
