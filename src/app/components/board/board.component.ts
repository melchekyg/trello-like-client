import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material';

import { Board } from './../../interfaces/board.interface';

import { DataService } from './../../services/data.service';

import { AddListDialogComponent } from './../../dialogs/add-list-dialog/add-list-dialog.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  board: Board = {
    lists: [],
    _id: this.dataService.uuidv4()
  };

  constructor(private dataService: DataService, public dialog: MatDialog) { 

  }

  ngOnInit() {
    this.dataService.onListChanges.subscribe(lists => {
      this.board.lists = lists
    })
  }

  addList() {
    const dialogRef = this.dialog.open(AddListDialogComponent);

    dialogRef.afterClosed().subscribe(({title, content}) => {
      this.dataService.addList({ _id: this.dataService.uuidv4(), title: title, cards: []})
    });
  }
}
