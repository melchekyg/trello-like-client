import { Component, OnInit, Input } from '@angular/core';

import {MatDialog} from '@angular/material';

import { List } from './../../interfaces/list.interface';

import { DataService } from './../../services/data.service'

import { AddCardDialogComponent } from './../../dialogs/add-card-dialog/add-card-dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() list: List;
  constructor(public dialog: MatDialog, private dataService: DataService) { }

  ngOnInit() {
  }

  addCard() {
    const dialogRef = this.dialog.open(AddCardDialogComponent);

    dialogRef.afterClosed().subscribe(({title, content}) => {
      this.dataService.addCard(this.list, { _id: this.dataService.uuidv4(), title: title, content: content})
    });
  }
  addDropItem({_id, content, title}) {
    this.dataService.addCard(this.list, {_id, content, title})
  }

  releaseDrop({_id, content, title}) {
    this.dataService.removeCard(this.list, {_id, content, title})
  }
}
