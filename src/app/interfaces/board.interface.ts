import { List } from './list.interface'

export interface Board {
    _id: string
    lists: List[]
}