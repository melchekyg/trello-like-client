export interface Card {
    _id: string;
    title: string;
    content: string;
}