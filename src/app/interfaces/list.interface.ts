import { Card } from './card.interface'

export interface List {
    _id: string,
    title: string,
    cards: Card[]
}